# cthulhunetes
2019-07-16

The cluster manager from nether dimensions. Come hear its infernal piping...

* Eldritch Dimensional Expansion
* Unspeakable Scaling

## use

    $ cthulctl
	cthulctl> call up
	...
	cthulctl> put down

Remember! Never call up what thou cannot put down.

The many-faced controller from beyond space:

	alias ktulctl=cthulctl
	
## timeline

Coming soon (for those for whom geological ages are but an eye-blink); at the end of the next dark aeon.

## history and origin

Here we see the original developers of c10s along with an early _interdimensional slicer_.

![Lovecraft & Thompson](images/lovecraft-thompson-pgp.png)

Later, celebrating their first consumed solar system.

![Ritchie & Ken Lovecraft](images/unix-lamp-lovecraft.png)

## where's the source

Obviously you cannot see into enough dimensions to observe it. Move along.